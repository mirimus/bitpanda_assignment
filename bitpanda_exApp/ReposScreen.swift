//
//  ViewController.swift
//  bitpanda_exApp
//
//  Created by Michael Irimus on 20.05.19.
//  Copyright © 2019 Michael Irimus. All rights reserved.
//

import UIKit

//https://api.github.com/v3

class ReposScreen: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //pass the whole object, care that we need a items specification -> covered in Struct above
        dataRequest(with: apiURL, objectType: JObj.self) { (result: Result) in
            switch result {
            case .success(let objects):
                //moved from Definition of function, may be overthought
                jsonResponse = objects
                
                //just console output
                print("Total Count: " + String(objects.total_count))
                print("Incomplete Results: " + String(objects.incomplete_results))
                print("Items: " + String(objects.items.count) + "\n")
                
                for concreteObj in objects.items {
                    print("NAME: " + concreteObj.name)
                    print("FULL NAME: " + concreteObj.full_name)
                    print("OWNER: " + concreteObj.owner.login + "\n")
                }
                //just console output
                
                //generate async tableView Connections here, otherwise cells retrieve nil data
                self.generateTableViewConnections()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func generateTableViewConnections(){
        DispatchQueue.main.async{
            self.tableView.delegate = self
            self.tableView.dataSource = self
        
            self.tableView.reloadData()
        }
    }
    
    //passing arguments to other viewcontroller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailScreenViewController = segue.destination as? DetailScreenViewController,
            let index = tableView.indexPathForSelectedRow?.row
            else {
                return
        }
        detailScreenViewController.item = jsonResponse.items[index]
    }
}

//adjust tableview dynamically on the fly
extension ReposScreen: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if jsonResponse.items.count < 25{
            return jsonResponse.items.count
        } else {
            return 25
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = jsonResponse.items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepoItemCell") as! RepoItemCell
        cell.setCellLabels(repo: item)
        
        return cell
    }
}

