//
//  DetailScreenViewController.swift
//  bitpanda_exApp
//
//  Created by Michael Irimus on 21.05.19.
//  Copyright © 2019 Michael Irimus. All rights reserved.
//

import UIKit

class DetailScreenViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var item : JObj.Items!
    var contributorsURL : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contributorsURL = "https://api.github.com/repos/"+item.owner.login+"/"+item.name+"/contributors"
        saveContributors(url: contributorsURL)
        // Do any additional setup after loading the view.
    }
    
    func generateTableViewConnections2(){
        DispatchQueue.main.async{
            self.tableView.delegate = self
            self.tableView.dataSource = self
            
            self.tableView.reloadData()
        }
    }
    
    //fetch the contributers json based on the parameters passed from the first viewcontroller
    func saveContributors(url: String){
        dataRequest(with: url, objectType: [ContributorsObj].self) { (result: Result) in switch result {
            case .success(let objects):
                contributorsObj = objects
                print(contributorsObj!)
                //self.generateTableViewConnections2()
                DispatchQueue.main.async{
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    
                    self.tableView.reloadData()
            }
            case .failure(let error):
                print(error)
            }
        }
    }
}

//return one Cell for the Detail page
extension DetailScreenViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let it = item!
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepoDetailCell") as! RepoDetailCell
        cell.setRepoDetails(repo: it)
        
        return cell
    }
}
