//
//  RepoDetailCell.swift
//  bitpanda_exApp
//
//  Created by Michael Irimus on 21.05.19.
//  Copyright © 2019 Michael Irimus. All rights reserved.
//

import Foundation
import UIKit

class RepoDetailCell: UITableViewCell{
    
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var contributorLabel: UILabel!
    
    func setRepoDetails(repo: JObj.Items){
        detailLabel.text = "Size: " + String(repo.size) + ", Stargazers: " + String(repo.stargazers_count) + ", Forks: " + String(repo.forks_count)
        contributorLabel.text = "Contributors: "
        for i in 0..<contributorsObj.count{
            contributorLabel.text?.append(contributorsObj[i].login)
        }
    }
}
