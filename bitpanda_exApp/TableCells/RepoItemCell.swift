//
//  ViewCell.swift
//  bitpanda_exApp
//
//  Created by Michael Irimus on 21.05.19.
//  Copyright © 2019 Michael Irimus. All rights reserved.
//

import UIKit

class RepoItemCell: UITableViewCell {

    @IBOutlet weak var tableViewLabel: UILabel!
    
    func setCellLabels(repo: JObj.Items){
        tableViewLabel.text = repo.name + ",       " + repo.full_name
    }
}
