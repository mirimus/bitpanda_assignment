//
//  JObj.swift
//  bitpanda_exApp
//
//  Created by Michael Irimus on 21.05.19.
//  Copyright © 2019 Michael Irimus. All rights reserved.
//

import Foundation
import UIKit

struct JObj: Decodable {
    let total_count: Int
    let incomplete_results: Bool
    
    var items = Array<Items>()
    struct Items: Decodable{
        let name: String
        let full_name: String
        let contributors_url: String
        let size: Int
        let forks_count: Int
        let stargazers_count: Int
        let owner : Owner
    }
    
    struct Owner: Decodable {
        let login: String
        let id: Int
    }
}
