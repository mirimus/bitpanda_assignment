//
//  ContributorsObj.swift
//  bitpanda_exApp
//
//  Created by Michael Irimus on 22.05.19.
//  Copyright © 2019 Michael Irimus. All rights reserved.
//

import Foundation

struct ContributorsObj: Decodable {
    let login: String
    let avatar_url: String
}
