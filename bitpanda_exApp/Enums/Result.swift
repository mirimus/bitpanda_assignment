//
//  Result.swift
//  bitpanda_exApp
//
//  Created by Michael Irimus on 21.05.19.
//  Copyright © 2019 Michael Irimus. All rights reserved.
//

import Foundation

//Result enum to show success or failure
enum Result<T> {
    case success(T)
    case failure(APPError)
}
